#!/usr/bin/env bash
set -euo pipefail

while true; do
    if [[ "$(playerctl -l | grep spotify)" == "spotify" ]]
    then
        player_status=$(playerctl -p spotify status)
        artist=$(playerctl -p spotify metadata artist)
        title=$(playerctl -p spotify metadata title)
        if [[ "$player_status" == "Playing" ]] ; then player_status="" ; else player_status="" ; fi
    fi
    load_avg=$(uptime | grep -a "load av*" | cut -f10-14 -d',' -d' ');
    xsetroot -name "$(echo "$artist/$title [$player_status]")|$(echo "$load_avg")|$(date)"
    sleep 1
done
